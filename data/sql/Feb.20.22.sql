START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `x_com`
--

CREATE TABLE `catalog`
(
    `id`                INT(10) UNSIGNED PRIMARY KEY AUTO_INCREMENT NOT NULL,
    `name`              VARCHAR(128) KEY,
    `parent_catalog_id` INT(10) UNSIGNED                            NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_unicode_ci;

CREATE TABLE `product_type`
(
    `id`   INT(10) UNSIGNED PRIMARY KEY AUTO_INCREMENT NOT NULL,
    `name` VARCHAR(128) UNIQUE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_unicode_ci;

CREATE TABLE `product`
(
    `id`              INT(10) UNSIGNED PRIMARY KEY AUTO_INCREMENT NOT NULL,
    `catalog_id`      INT(10) UNSIGNED                            NULL,
    `product_type_id` INT(10) UNSIGNED                            NULL,
    `name`            VARCHAR(256) KEY,
    `vendor_code`     VARCHAR(64) KEY,
    `image_name`      VARCHAR(256) COMMENT '[Путь/][Название]',
    `description`     TEXT
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_unicode_ci;

