<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

define('X_COM_MINIMAL_VERSION_REQIURED', '7.1.33');

if (version_compare(PHP_VERSION, X_COM_MINIMAL_VERSION_REQIURED) < 0) {
    echo 'Необходима PHP версия не ниже "' . X_COM_MINIMAL_VERSION_REQIURED
         . '", текущая версия: ' . PHP_VERSION . "\n";
    exit;
}

$selNumberTag = 'attr_' . ($_GET['sel'] ?? 0);
$$selNumberTag = ' class="selected" ';

?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>X-COM tests</title>
    <style>
        .main-content {
            width: 80%;
            padding: 10px 30px;
            margin: 10px auto;
            background-color: #dac082;
        }
        .selected {
            background-color: black;
            color: white;
        }
        table td {
            padding: 10px;
            background-color: beige;
        }
    </style>
</head>
<body>
<div class="main-content">
    <div>
        <table>
            <tr><td colspan="3">Выберите:</td></tr>
            <tr>
                <td>
                    <a <?= $attr_1 ?? '' ?> href="task_1_safety.php?sel=1">Задание 1. Безопасность</a>
                </td>
                <td>
                    <a <?= $attr_2 ?? '' ?> href="task_2_SQL.php?sel=2">Задание 2. SQL</a>
                </td>
                <td>
                    <a <?= $attr_3 ?? '' ?> href="task_3_code.php?sel=3">Задание 3. Код</a>
                </td>
            </tr>
        </table>
